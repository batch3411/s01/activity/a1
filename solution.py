# 1. Create 5 variables and output them in the command prompt:

name = "Han"
age = 23
occupation = "Data Analyst"
movie = "Sherlock Holmes"
rating = 95

print(f"I am {name}, and I am {age} years old. I work as a {occupation} and my rating for the movie {movie} is {rating}%.")

# 2. Create 3 variables, num1, num2, and num3

num1, num2, num3 = 22, 14, 25

print(num1 * num2)
print(num1 < num3)
print(num2 + num3)
